import sqlite3 as sq
conn = sq.connect("./data/data.db")
db = conn.cursor()
db.execute("SELECT * FROM wifinetwork")

ssid = {}

while True:
    row = db.fetchone()
    if row == None:
        break;

    theSSID = row[2][1:-1]

    if theSSID not in ssid:
        ssid[theSSID] = []

    if row[1] not in ssid[theSSID]:
        ssid[theSSID].append(row[1])

# ssid to user
user = []
for s in ssid:
    user.append(ssid[s])

# sort stuff
user.sort(key=lambda a: len(a))
userlist = []

for uuids in user:
    for uuid in uuids:
        if uuid in userlist:
            uuids.remove(uuid)
            if len(uuids) == 0:
                user.remove(uuids)
        else:
            userlist.append(uuid)

# print
for i in range(0, len(user)):
    print("%d: %s" % (i, repr(user[i])))

conn.close()
