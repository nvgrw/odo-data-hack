#ODO Data Hack
Using the data collected by the [King's College Big Social Data MobileMiner](http://kingsbsd.github.io/MobileMiner) I created this in a very short hackathon.

The problem with the MobileMiner was that after restoring a device, the app assigned a new user ID to a phone. This meant that many users actually had multiple user IDs and there was no way of determining what user IDs linked to what user. This hack _kinda sorta_ tries to rectify this  issue by using the collected WiFi SSID data to link user IDs to users.

It's still very much a work in progress, and unfortunately may remain such for the foreseeable future.

Currently all it does is list the user IDs in order of length per user. The longest entries tend to be linked to public WiFi networks and need to be handled separately at some point, which will probably require some data about the SSIDs of public WiFi hotspots.